import java.io.File
import java.util.Scanner
import kotlin.math.max

/*
Ex 1: Frea kit
He de fer varies coses no molt complicades pero si laboriosas, molt trball he d'anar per feina així que primer faré el menu gran i dsprés paso al subbmenu,
tractaré les dades desde un fitxer serà més senzill
 */
fun mostrarMenu(){
    println("1- Mostrar dades llegides de l’activitat.\n" +
            "2- Mostrar quantitat mesures de l’activitat\n" +
            "3- Mostrar freqüència cardíaca mitjana de l’activitat\n" +
            "4- Mostrar freqüències cardíaques mínima i màxima de l’activitat\n" +
            "5- Aprofitament de l’activitat (aquesta opció mostrarà un submenú)\n" +
            "0- Sortir de l’aplicació")

}
fun mostrarSubMenu () {
    println("1- Introduir edat usuari\n" +
            "2- Nivell molt suau: mostrar número de lectures i % entre el 50% i el 59% de la FCM.\n" +
            "3- Nivell suau: mostrar número de lectures i % entre el 60% i el 69% de la FCM.\n" +
            "4- Nivell moderat: mostrar número de lectures i % entre el 70% i el 79% de la FCM.\n" +
            "5- Nivell intens: mostrar número de lectures i % entre el 80% i el 89% de la FCM.\n" +
            "6- Nivell màxim: mostrar número de lectures i % entre el 90% i el 100% de la FCM.\n" +
            "7- Mostrar lectures per sota del nivell suau\n" +
            "8- Mostrar lectures per sobre de la FCM\n" +
            "0- Tornar al menú principal\n")
}
fun fileSize (file:String):Int {
    val lines =  file.split(" ")
    return lines.size
}
fun mitjanaCalculate (file: String):Int {
    val lines = file.split(" ")
    var mitjana = 0
    for (i in lines){
        mitjana += i.toInt()
    }
    return (mitjana/lines.size)
}
fun orderNumbers(lines: MutableList<String>): MutableList<Int> {
    //clean
    val numbers : MutableList<Int> = mutableListOf()
    for (i in lines){
        numbers.add(i.toInt())
    }
    var corredor=0
    var first=0
    var second=1
    do{
        first=0
        second=1
        corredor=0
        for ( i in 0 until numbers.lastIndex){
            if (numbers[first].toInt()>numbers[second].toInt()){
                val save=numbers[second].toInt()
                numbers[second]=numbers[first]
                numbers[first]= save
                first++
                second++
                corredor++
            }else{
                first++
                second++
            }
        }
    }while (corredor!=0)
    return numbers
}
fun minOfNumbers (file: String): Int {
    val lines = file.split(" ").toMutableList()
    val numbers = orderNumbers(lines)

    return numbers[0]
}
fun maxOfNumbers (file: String):Int {
    val lines = file.split(" ").toMutableList()
    val numbers = orderNumbers(lines)

    return numbers[numbers.lastIndex]
}
fun numbersOfFCM (edat : Int ,file: String, minP: Int, maxP:Int):MutableList<Int> {
    val FCM = 220 - edat
    val FCMonMaxP:Double = (maxP.toDouble()/100) * FCM.toDouble()
    val FCMonMinP:Double = (minP.toDouble()/100) * FCM.toDouble()
    val numbers = orderNumbers(file.split(" ").toMutableList())
    val numbersOnRange = mutableListOf<Int>()
    for (i in numbers){
        if (i in (FCMonMinP.toInt() + 1) until FCMonMaxP.toInt()){
            numbersOnRange.add(i)
        }
    }
    return numbersOnRange
}
fun numbersUnderFCM (edat : Int ,file: String, minP: Int, maxP:Int):MutableList<Int> {
    val FCM = 220 - edat
    val FCMonMinP:Double = (minP.toDouble()/100) * FCM.toDouble()
    val numbers = orderNumbers(file.split(" ").toMutableList())
    val numbersOnRange = mutableListOf<Int>()
    for (i in numbers){
        if (i < FCMonMinP){
            numbersOnRange.add(i)
        }
    }
    return numbersOnRange
}
fun numbersOverFCM (edat : Int ,file: String, minP: Int, maxP:Int):MutableList<Int> {
    val FCM = 220 - edat
    val FCMonMaxP:Double = (maxP.toDouble()/100) * FCM.toDouble()
    val numbers = orderNumbers(file.split(" ").toMutableList())
    val numbersOnRange = mutableListOf<Int>()
    for (i in numbers){
        if (i > FCMonMaxP){
            numbersOnRange.add(i)
        }
    }
    return numbersOnRange
}
fun calcualtePorcentatge (edat : Int ,file: String, minP: Int, maxP:Int):Double {
    val numbers = numbersOfFCM(edat,file,minP,maxP)
    return numbers.size.toDouble()*100/(fileSize(file).toDouble())
}
fun main() {
    val file = File("src/main/kotlin/data.txt").readText()
    val scanner = Scanner(System.`in`)
    var userAge:Int = 0
    do {
        mostrarMenu()
        val userInput = scanner.nextInt()
        when (userInput){
            1-> println(file)
            2-> println(fileSize(file))
            3-> println(mitjanaCalculate(file))
            4-> println("${minOfNumbers(file)}  ${maxOfNumbers(file)}")
            5-> {
                do {
                    mostrarSubMenu()
                    val submenuInput = scanner.nextInt()
                    when (submenuInput){
                        1-> {
                            print("Introdueix la edat:")
                            userAge = scanner.nextInt()
                        }
                        2->{
                            val numbersInRange = numbersOfFCM(userAge, file,50,59 )
                            println("Nombres dins el porcentatge:")
                            for (i in numbersInRange){
                                print("$i ")
                            }
                            println()
                            println("porcentatge de nombres respecte el total:")
                            println(calcualtePorcentatge(userAge,file,50,59))
                        }
                        3 ->{
                            val numbersInRange = numbersOfFCM(userAge, file,60,69 )
                            println("Nombres dins el porcentatge:")
                            for (i in numbersInRange){
                                print("$i ")
                            }
                            println()
                            println("porcentatge de nombres respecte el total:")
                            println(calcualtePorcentatge(userAge,file,60,69))
                        }
                        4->{
                            val numbersInRange = numbersOfFCM(userAge, file,70,79 )
                            println("Nombres dins el porcentatge:")
                            for (i in numbersInRange){
                                print("$i ")
                            }
                            println()
                            println("porcentatge de nombres respecte el total:")
                            println(calcualtePorcentatge(userAge,file,70,79))
                        }
                        5->{
                            val numbersInRange = numbersOfFCM(userAge, file,80,89 )
                            println("Nombres dins el porcentatge:")
                            for (i in numbersInRange){
                                print("$i ")
                            }
                            println()
                            println("porcentatge de nombres respecte el total:")
                            println(calcualtePorcentatge(userAge,file,80,89))
                        }
                        6->{
                            val numbersInRange = numbersOfFCM(userAge, file,90,100 )
                            println("Nombres dins el porcentatge:")
                            for (i in numbersInRange){
                                print("$i ")
                            }
                            println()
                            println("porcentatge de nombres respecte el total:")
                            println(calcualtePorcentatge(userAge,file,90,100))
                        }
                        7->{
                            val numbersInRange = numbersUnderFCM(userAge, file,50,100 )
                            println("Nombres dins el porcentatge:")
                            for (i in numbersInRange){
                                print("$i ")
                            }
                            println()
                        }
                        8->{
                            val numbersInRange = numbersOverFCM(userAge, file,50,100 )
                            println("Nombres dins el porcentatge:")
                            for (i in numbersInRange){
                                print("$i ")
                            }
                            println()
                        }
                    }
                }while (submenuInput != 0)

            }
        }
    }while (userInput != 0)
}