//definició dels colors que farem servir, agafant els backgrounds ANSI, i establint-los com a constants globals
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"

fun changeColor (color:Int, colors:Array<String>):Int {
    var colorValue = color
    if (color!=colors.size-1 && color != 0) colorValue++
    else if(color==colors.size-1 && color != 0) colorValue=1
    return colorValue
}
fun main(){
    val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    var color=1
    var intensitat=1
    var intensitatPujada=true
    var offColor = color
    do {
        print("Introdueix ordre: ")
        val instruction= readln().uppercase()
        when(instruction) {
            "TURN ON" -> {
                if(color==0){
                    color=offColor
                    intensitat=1
                    intensitatPujada=true
                }
            }
            "TURN OFF" -> {
                if(color>0){
                    offColor=color
                    color=0
                    intensitat=0
                }
            }
            "CHANGE COLOR" -> {
                color = changeColor(color, colors)
            }
            "INTENSITY" -> {
                if (intensitatPujada && color != 0) intensitat++
                else if (intensitatPujada && intensitat==5 && color != 0){
                    intensitatPujada=false
                    intensitat--
                }
                else if (!intensitatPujada && intensitat>1) intensitat--
                else if (!intensitatPujada && intensitat==1 && color != 0){
                    intensitatPujada = true
                    intensitat++
                }
            }
        }
        println("Color: ${colors[color]}  $ANSI_RESET - intensitat $intensitat $color")
    } while(instruction!="END")
}