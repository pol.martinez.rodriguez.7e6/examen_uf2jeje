import java.util.Scanner

/*
Sequencia d'asteriscos
 */
fun asteriscSequence (num:Int) {
    if (num>1) asteriscSequence(num-1)
    if (num>1) asteriscSequence(num-1)
    for (i in 1..num){
        print("*")
    }
    println()
}

fun main () {
    val scanner = Scanner(System.`in`)
    println("Introdueix la llargada de la sequencia")
    val large = scanner.nextInt()
    asteriscSequence(large)
}