import java.util.Scanner
import kotlin.math.exp

fun decimalToBynari (decimal: Int):String {
    return if (decimal!= 0) decimalToBynari(decimal/2) + decimal % 2
    else " "

}
fun digitCounter (primerNumero:Int):Int {
    if (primerNumero > 0) return 1 + digitCounter(primerNumero/10)
    else return 0
}
fun converterDecimal (binary:Int):Int{
    var decimal = 0.0
    var binary = binary
    var exponent = 0.0
    var decimalOnConvers = binary
    for (i in 1.. digitCounter(binary)){
        decimalOnConvers = binary % 10
        binary /= 10
        decimal += decimalOnConvers * Math.pow(2.0,exponent)
        exponent += 1.0
    }
    return decimal.toInt()
}

fun main() {
    val scanner = Scanner(System.`in`)
    var number = scanner.nextInt()
    println("Introdueix un numero")

    println(converterDecimal(number))
    println(decimalToBynari(number))
}